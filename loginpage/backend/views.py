from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from loginpage.models import Logintasks
from django.contrib.messages import constants as messages


@login_required()
def loginpage(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = Logintasks.authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return redirect(request=request, template_name='tasksmanagement/adminlogin.html')
        else:
            messages.INFO('request, Username or Password is incorrect')

        context = {}
        return render(request, 'tasksmanagement/loginpage.html', context)


