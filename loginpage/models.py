from django.db import models


class Logintasks(models.Model):
    username = models.CharField(max_length=100, null=False, blank=False)
    password = models.CharField(max_length=50, null=False, blank=False)

    def __str__(self):
        return "{}".format(self.username)

    @classmethod
    def authenticate(cls, request, username, password):
        pass

