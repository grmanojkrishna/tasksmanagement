from django.contrib import admin
from . import models


@admin.register(models.Logintasks)
class LoginAdmin(admin.ModelAdmin):
    list_display = ('username', 'password')

