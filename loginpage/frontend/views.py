from django.shortcuts import render
from django.contrib.auth.decorators import login_required


@login_required
def logintasks(request):

    return render(request=request, template_name="tasksmanagement/loginpage.html")
