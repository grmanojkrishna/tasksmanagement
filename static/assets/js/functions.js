Date.prototype.toDateInputValue = (function() {
	var local = new Date(this);
	local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
	return local.toJSON().slice(0,10);
});

Date.prototype.addDays = function(days) {
    this.setDate(this.getDate() + parseInt(days));
    return this;
};

function format_date(string){
	const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	var date = new Date(string);
	return monthNames[date.getMonth()] + " " + date.getDate() + ", " + date.getFullYear() + " " //+ date.toLocaleTimeString()
}

function format_time(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

function format_date_time(string){
	const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	var date = new Date(string);
	return monthNames[date.getMonth()] + " " + date.getDate() + ", " + date.getFullYear() + " " + format_time(date);
}

function format_number(x){
	if(x==null) return 0;
  sign=0;
  if(x<0){
    sign=1;
  }
  x=Math.abs(x);
  y=x;
  x=x.toString();
	if(x.indexOf('.') > 0){
       y = parseFloat(y).toFixed(2);
       console.log(y);
       x=y.toString();
       console.log(x);
  }
	var afterPoint = '';
	if(x.indexOf('.') > 0)
		afterPoint = x.substring(x.indexOf('.'),x.length);
	x = Math.floor(x);
	x=x.toString();
	var lastThree = x.substring(x.length-3);
	var otherNumbers = x.substring(0,x.length-3);
	if(otherNumbers != '')
		lastThree = ',' + lastThree;
	var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
  if(sign==0){
    return res;
  }
  else{
    return "-" + res;
  }
}

function load_format_number(){
    for(span of document.getElementsByClassName("format_number")){
        span.innerHTML = format_number(span.innerHTML);
    }
}

function get_bool_icon(value){
	if (value){
		return `<span class="tag is-success"><i class="fa fa-check" aria-hidden="true"></i></span>`;
	}
	else {
		return `<span class="tag is-warning"><i class="fa fa-clock" aria-hidden="true"></i></span>`;
	}
}

function get_json(url, callback=null, error_callback=null){
	var xhr = new XMLHttpRequest();
	xhr.open('GET', url, true);
	xhr.onload = function(){
		if(this.readyState === 4){
			if(this.status === 200 || this.status === 201){
				try{
					var data = JSON.parse(this.responseText);
					if(callback !== null){ callback(data); }
				}
				catch(e){
					var data = this.responseText;
					if(callback !== null){ callback(data); }
				}
			}
			else{
				alert('try again!');
				try{
					var data = JSON.parse(this.responseText);
					if(error_callback !== null){ error_callback(data); }
				}
				catch(e){
					var data = this.responseText;
					if(error_callback !== null){ error_callback(data); }
				}
			}
		}
	}
	xhr.setRequestHeader("x-skip-middleware", "true");
	xhr.send();
}

function post_json(url, post_data, callback=null, error_callback=null){
	var xhr = new XMLHttpRequest();
	xhr.open('POST', url, true);
	xhr.onload = function(){
		if(this.readyState === 4){
			if(this.status === 200 || this.status === 201){
				try{
					var data = JSON.parse(this.responseText);
					if(callback !== null){ callback(data); }
				}
				catch(e){
					var data = this.responseText;
					if(callback !== null){ callback(data); }
				}
			}
			else{
				alert('try again!');
				try{
					var data = JSON.parse(this.responseText);
					if(error_callback !== null){ error_callback(data); }
				}
				catch(e){
					var data = this.responseText;
					if(error_callback !== null){ error_callback(data); }
				}
			}
		}	
	}
	xhr.setRequestHeader("x-skip-middleware", "true");
	xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	xhr.send(JSON.stringify(post_data));
}


function get_selected_option_value(select_id){
	var e = document.getElementById(select_id);
	if (e.options[e.selectedIndex].value == "null"){
		return null;
	}
	else{
		return e.options[e.selectedIndex].value;	
	}
}

function delete_selected_table_rows(table_id) {
	var table = document.getElementById(table_id);
	var rowCount = table.rows.length;
	for(var i=0; i<rowCount; i++) {
		var row = table.rows[i];
		var chkbox = row.cells[0].childNodes[0];
		if(null != chkbox && true == chkbox.checked) {
			table.deleteRow(i);
			rowCount--;
			i--;
			window.new_fabric_shade_count -= 1;
		}
	}
}

function set_select_options(select_id, url, name_field = null){
  $.getJSON(url,function(retrieved_data){
    var select = document.getElementById(select_id);
    while (select.hasChildNodes()) {
      select.removeChild(select.lastChild);
    }
    var option = document.createElement("option");
    option.value = "-1";
    option.text = "select"
    select.appendChild(option);
    for (i in retrieved_data){
      var option = document.createElement("option");
      option.value = retrieved_data[i][0];
      option.text = retrieved_data[i][1];
      select.appendChild(option);
    }
    $('#'+select_id).select2();
  });
}


function calculate_amount(row_id){
  var result = 0;
  if(document.getElementById('quantity'+row_id) !== null){
    result += +document.getElementById('quantity'+row_id).value;
  }
  if(document.getElementById('rate'+row_id) !== null){
    result = result * +document.getElementById('rate'+row_id).value;
  }
  if(document.getElementById('amount'+row_id) !== null){
    document.getElementById('amount'+row_id).value = result;
  }
}

function calculate_total(){
  var total_count = 0;
  var result = 0;
  for (var i=0; i<=rowId; i++){
    if(document.getElementById('amount'+i) !== null){
      result += +document.getElementById('amount'+i).value;
    }
    else if(document.getElementById('quantity'+i) !== null){
      result += +document.getElementById('quantity'+i).value;
    }
    if(document.getElementById('quantity'+i) !== null){
      total_count += +document.getElementById('quantity'+i).value;
    }
  }
  if(document.getElementById('total_quantity') !== null){
    document.getElementById('total_quantity').value = total_count;
  }
}
function calculate_total_new(elementId,quantityId){
  var total_count = 0;
  var result = 0;
  for (var i=0; i<=rowId; i++){
    if(document.getElementById('amount'+i) !== null){
      result += +document.getElementById('amount'+i).value;
    }
    else if(document.getElementById(quantityId+i) !== null){
      result += +document.getElementById(quantityId+i).value;
    }
    if(document.getElementById(quantityId+i) !== null){
      total_count += +document.getElementById(quantityId+i).value;
    }
  }
  if(document.getElementById(elementId) !== null){
    document.getElementById(elementId).value = total_count;
  }
}

function calculate_total_value(){
  var result = 0;
  for (var i=0; i<=rowId; i++){
    if(document.getElementById('amount'+i) !== null){
      result += +document.getElementById('amount'+i).value;
    }
  }
  if(document.getElementById('total_value') !== null){
    document.getElementById('total_value').value = result;
  }
}
function add_row(table_id, table_data){

  $('.add_shade_btn').hide();
  $('.table_input_field').prop("readonly",true);

  console.log("add row");

  var table = document.getElementById(table_id);
  var row = table.insertRow(table.rows.length);
  row.id = "row" + rowId.toString();
  var field, element, div, cellCount = 0;

  field = row.insertCell(cellCount++);
  element = document.createElement("input");
  element.type = "checkbox";
  element.className += '';
  field.appendChild(element);
  element = null;
  if(table_data.type === true){
    field = row.insertCell(cellCount++);
    element = document.createElement("select");
    element.name = `line[${rowId}]` + "[type]";
    element.className += 'form-control select2 table_select';
    element.id = "type" + rowId;
    element.required = true;
    element.style.width = "150px";
    $(document.body).on("change","#type" + rowId,function(){
      var option = this.value;
      var thisrow = this.id.substring(4);
      if(table_data.item === true){
        set_select_options("item" + thisrow, '/api/master/get-items/?type='+option);
      }
    });
    field.appendChild(element);
    element = null;
    set_select_options("type" + rowId, '/api/master/get-types/');
  }

  if(table_data.product === true){
    field = row.insertCell(cellCount++);
    element = document.createElement("select");
    element.name = `line[${rowId}]` + "[product]";
    element.className += 'form-control select2 table_select';
    element.id = "product" + rowId;
    element.required = true;
    element.width = "400px";
    $(document.body).on("change","#product" + rowId,function(){
      var option = this.value;
      var thisrow = this.id.substring(4);
      if(table_data.product === true){
        set_select_options("product" + thisrow, '/api/master/get-products/?product='+option);
      }
    });
    field.appendChild(element);
    element = null;
    set_select_options("product" + rowId, '/api/master/get-products/');
  }

  if(table_data.item === true){
    field = row.insertCell(cellCount++);
    element = document.createElement("select");
    element.name = `line[${rowId}]` + "[item_id]";
    element.className += 'form-control select2 table_select ';
    element.id = "item" + rowId;
    element.required = true;
    element.style.width = "250px";
    $(document.body).on("change","#item" + rowId,function(){
      var option = this.value;
      var thisrow = this.id.substring(4);
      if(table_data.shade === true){
        set_select_options("shade" + thisrow, '/api/master/get-shades/?item_id='+option);
      }
    });
    field.appendChild(element);
    element = null;
  }
  if(table_data.shade === true){
    field = row.insertCell(cellCount++);
    element = document.createElement("select");
    element.name = `line[${rowId}]` + "[shade_id]";
    element.className += 'form-control select2 table_select';
    element.id = "shade" + rowId;
    element.required = true;
    element.style.width = "120px";
    $(document.body).on("change","#shade" + rowId,function(){
      var thisrow = this.id.substring(5);
      if(table_data.add_shade_btn === true){
        document.getElementById("btn"+thisrow).disabled = false;
      }
    });
    field.appendChild(element);
    element = null;

    if(table_data.add_shade_btn === true){
      field = row.insertCell(cellCount++);
      element = document.createElement("button")
      element.innerHTML = "+";
      element.type = "button";
      element.className += "btn btn-success add_shade_btn";
      element.id = "btn" + rowId;
      element.disabled = true;
      element.addEventListener("click", function(event){
        event.preventDefault();
        var thisrow = this.id.substring(3);
        add_shade_row(table_id, table_data, thisrow);
      });
      field.appendChild(element);
    }

  }
  if(table_data.warp_calculation === true){
    field = row.insertCell(cellCount++);
    element = document.createElement("input");
    element.type = "text";
    element.name = `line[${rowId}]` + "[ends]";
    element.className += 'form-control table_input_field ';
    element.placeholder = "Ends"
    element.id = "ends" + rowId;
    element.style.width = "80px";
    element.addEventListener("keyup", function(event) {
      var thisrow = this.id.substring(4);
      if (table_data.polyester_warp === true){
        calculate_polyester_warp_expected(thisrow);
      }
      else{
        calculate_warp_expected_qty(thisrow);
      }
    });
    field.appendChild(element);
    element = null;

    field = row.insertCell(cellCount++);
    element = document.createElement("input");
    element.type = "text";
    element.name = `line[${rowId}]` + "[count]";
    element.className += 'form-control table_input_field ';
    element.placeholder = "Count"
    element.id = "count" + rowId;
    element.style.width = "65px";
    element.addEventListener("keyup", function(event) {
      var thisrow = this.id.substring(5);
      if (table_data.polyester_warp === true){
        calculate_polyester_warp_expected(thisrow);
      }
      else{
        calculate_warp_expected_qty(thisrow);
      }
    });
    field.appendChild(element);
    element = null;

    field = row.insertCell(cellCount++);
    element = document.createElement("input");
    element.type = "text";
    element.name = `line[${rowId}]` + "[exp_qty]";
    element.className += 'form-control table_input_field ';
    element.placeholder = "Expected"
    element.id = "exp_qty" + rowId;
    element.readOnly = true;
    element.style.width = "85px";
    element.addEventListener("keyup", function(event) {
      var thisrow = this.id.substring(7);
      if (table_data.polyester_warp === true){
        calculate_polyester_warp_expected(thisrow);
      }
      else{
        calculate_warp_expected_qty(thisrow);
      }
    });
    field.appendChild(element);
    element = null;
  }
  if(table_data.quantity === true){
    field = row.insertCell(cellCount++);
    element = document.createElement("input");
    element.type = "text";
    element.name = `line[${rowId}]` + "[quantity]";
    element.className += 'form-control table_input_field ';
    element.placeholder = "Quantity"
    element.id = "quantity" + rowId;
    element.required = true;
    element.style.width = "85px";
    element.addEventListener("keyup", function(event) {
      var thisrow = this.id.substring(8);
      calculate_amount(thisrow);
      calculate_total();
      calculate_total_value();
    });
    field.appendChild(element);
    element = null;
  }
  
  if(table_data.rate === true){
    field = row.insertCell(cellCount++);
    element = document.createElement("input");
    element.type = "text";
    element.name = `line[${rowId}]` + "[rate]";
    element.className += 'form-control table_input_field ';
    element.id = "rate" + rowId;
    element.required = true;
    element.style.width = "85px";
    element.value = 0;
    element.placeholder = "Rate";
    element.addEventListener("keyup", function(event) {
      if(event.keyCode === 13 || event.charCode === 13){
        event.preventDefault();

      }

      var thisrow = this.id.substring(4);
      calculate_amount(thisrow);
      calculate_total();
      calculate_total_value();
    });
    field.appendChild(element);
    element = null;
  }

  if(table_data.amount === true){
    field = row.insertCell(cellCount++);
    element = document.createElement("input");
    element.type = "text";
    element.name = `line[${rowId}]` + "[amount]";
    element.className += 'form-control table_input_field';
    element.id = "amount" + rowId;
    element.required = true;
    element.placeholder = "Amount";
    element.value = 0;
    element.style.width = "85px";
    element.readOnly = true;
    field.appendChild(element);
    element = null;
  }

  if(table_data.description === true){
    field = row.insertCell(cellCount++);
    element = document.createElement("input");
    element.type = "text";
    element.name = `line[${rowId}]` + "[description]";
    element.className += 'form-control';
    element.id = "description" + rowId;
    element.placeholder = "Description"
    field.appendChild(element);
    element = null;
  }
  if(table_data.cost_price === true){
    field = row.insertCell(cellCount++);
    element = document.createElement("input");
    element.type = "text";
    element.name = `line[${rowId}]` + "[cost]";
    element.className += 'form-control table_input_field';
    element.id = "cost" + rowId;
    element.required = true;
    element.placeholder = "Cost";
    element.value = table_data.cost_value;
    element.style.width = "85px";
    element.readOnly = true;
    field.appendChild(element);
    element = null;
  }
  rowId = rowId + 1;
}
function add_row_new(table_id, table_data){

  $('.add_shade_btn').hide();
  $('.table_input_field').prop("readonly",true);

  console.log("add row");

  var table = document.getElementById(table_id);
  var row = table.insertRow(table.rows.length);
  row.id = "row" + rowId.toString();
  var field, element, div, cellCount = 0;

  field = row.insertCell(cellCount++);
  element = document.createElement("input");
  element.type = "checkbox";
  element.className += '';
  field.appendChild(element);
  element = null;
  if(table_data.type === true){
    field = row.insertCell(cellCount++);
    element = document.createElement("select");
    element.name = `line[${rowId}]` + "[type]";
    element.className += 'form-control select2 table_select';
    element.id = "type" + rowId;
    element.required = true;
    element.style.width = "150px";
    $(document.body).on("change","#type" + rowId,function(){
      var option = this.value;
      var thisrow = this.id.substring(4);
      if(table_data.item === true){
        set_select_options("item" + thisrow, '/api/master/get-items/?type='+option);
      }
    });
    field.appendChild(element);
    element = null;
    set_select_options("type" + rowId, '/api/master/get-types/');
  }

  if(table_data.product === true){
    field = row.insertCell(cellCount++);
    element = document.createElement("select");
    element.name = `line[${rowId}]` + "[product]";
    element.className += 'form-control select2 table_select';
    element.id = "product" + rowId;
    element.required = true;
    element.width = "400px";
    $(document.body).on("change","#product" + rowId,function(){
      var option = this.value;
      var thisrow = this.id.substring(4);
      if(table_data.product === true){
        set_select_options("product" + thisrow, '/api/master/get-products/?product='+option);
      }
    });
    field.appendChild(element);
    element = null;
    set_select_options("product" + rowId, '/api/master/get-products/');
  }

  if(table_data.item === true){
    field = row.insertCell(cellCount++);
    element = document.createElement("select");
    element.name = `line[${rowId}]` + "[item_id]";
    element.className += 'form-control select2 table_select ';
    element.id = "item" + rowId;
    element.required = true;
    element.style.width = "250px";
    $(document.body).on("change","#item" + rowId,function(){
      var option = this.value;
      var thisrow = this.id.substring(4);
      if(table_data.shade === true){
        set_select_options("shade" + thisrow, '/api/master/get-shades/?item_id='+option);
      }
    });
    field.appendChild(element);
    element = null;
  }
  if(table_data.shade === true){
    field = row.insertCell(cellCount++);
    element = document.createElement("select");
    element.name = `line[${rowId}]` + "[shade_id]";
    element.className += 'form-control select2 table_select';
    element.id = "shade" + rowId;
    element.required = true;
    element.style.width = "120px";
    $(document.body).on("change","#shade" + rowId,function(){
      var thisrow = this.id.substring(5);
      if(table_data.add_shade_btn === true){
        document.getElementById("btn"+thisrow).disabled = false;
      }
    });
    field.appendChild(element);
    element = null;

    if(table_data.add_shade_btn === true){
      field = row.insertCell(cellCount++);
      element = document.createElement("button")
      element.innerHTML = "+";
      element.type = "button";
      element.className += "btn btn-success add_shade_btn";
      element.id = "btn" + rowId;
      element.disabled = true;
      element.addEventListener("click", function(event){
        event.preventDefault();
        var thisrow = this.id.substring(3);
        add_shade_row(table_id, table_data, thisrow);
      });
      field.appendChild(element);
    }

  }
  if(table_data.warp_calculation === true){
    field = row.insertCell(cellCount++);
    element = document.createElement("input");
    element.type = "text";
    element.name = `line[${rowId}]` + "[ends]";
    element.className += 'form-control table_input_field ';
    element.placeholder = "Ends"
    element.id = "ends" + rowId;
    element.style.width = "80px";
    element.addEventListener("keyup", function(event) {
      var thisrow = this.id.substring(4);
      if (table_data.polyester_warp === true){
        calculate_polyester_warp_expected(thisrow);
      }
      else{
        calculate_warp_expected_qty(thisrow);
      }
    });
    field.appendChild(element);
    element = null;

    field = row.insertCell(cellCount++);
    element = document.createElement("input");
    element.type = "text";
    element.name = `line[${rowId}]` + "[count]";
    element.className += 'form-control table_input_field ';
    element.placeholder = "Count"
    element.id = "count" + rowId;
    element.style.width = "65px";
    element.addEventListener("keyup", function(event) {
      var thisrow = this.id.substring(5);
      if (table_data.polyester_warp === true){
        calculate_polyester_warp_expected(thisrow);
      }
      else{
        calculate_warp_expected_qty(thisrow);
      }
    });
    field.appendChild(element);
    element = null;

    field = row.insertCell(cellCount++);
    element = document.createElement("input");
    element.type = "text";
    element.name = `line[${rowId}]` + "[exp_qty]";
    element.className += 'form-control table_input_field ';
    element.placeholder = "Expected"
    element.id = "exp_qty" + rowId;
    element.readOnly = true;
    element.style.width = "85px";
    element.addEventListener("keyup", function(event) {
      var thisrow = this.id.substring(7);
      if (table_data.polyester_warp === true){
        calculate_polyester_warp_expected(thisrow);
      }
      else{
        calculate_warp_expected_qty(thisrow);
      }
    });
    field.appendChild(element);
    element = null;
  }
  if(table_data.quantity === true){
       let quantityId = 'quantity'
      if ('quantityId' in table_data){
          quantityId = table_data.quantityId
      }
    field = row.insertCell(cellCount++);
    element = document.createElement("input");
    element.type = "text";
    element.name = `line[${rowId}]` + "[quantity]";
    element.className  += 'form-control table_input_field js_item-qty '  ;
    element.placeholder = "Quantity"
    element.id = quantityId + rowId;
    element.required = true;
    element.style.width = "85px";
    element.addEventListener("keyup", function(event) {
      var thisrow = this.id.substring(8);
      calculate_amount(thisrow);
        if ('total_id' in table_data){
            calculate_total_new(table_data.total_id,quantityId)
        }
        else {calculate_total_new('total_quantity')}
      calculate_total_value();
    });
    field.appendChild(element);
    element = null;
  }

  if(table_data.rate === true){
    field = row.insertCell(cellCount++);
    element = document.createElement("input");
    element.type = "text";
    element.name = `line[${rowId}]` + "[rate]";
    element.className += 'form-control table_input_field ';
    element.id = "rate" + rowId;
    element.required = true;
    element.style.width = "85px";
    element.value = 0;
    element.placeholder = "Rate";
    element.addEventListener("keyup", function(event) {
      if(event.keyCode === 13 || event.charCode === 13){
        event.preventDefault();

      }

      var thisrow = this.id.substring(4);
      calculate_amount(thisrow);
      calculate_total_new();
      calculate_total_value();
    });
    field.appendChild(element);
    element = null;
  }

  if(table_data.amount === true){
    field = row.insertCell(cellCount++);
    element = document.createElement("input");
    element.type = "text";
    element.name = `line[${rowId}]` + "[amount]";
    element.className += 'form-control table_input_field';
    element.id = "amount" + rowId;
    element.required = true;
    element.placeholder = "Amount";
    element.value = 0;
    element.style.width = "85px";
    element.readOnly = true;
    field.appendChild(element);
    element = null;
  }
  if(table_data.description === true){
    field = row.insertCell(cellCount++);
    element = document.createElement("input");
    element.type = "text";
    element.name = `line[${rowId}]` + "[description]";
    element.className += 'form-control';
    element.id = "description" + rowId;
    element.placeholder = "Description"
    field.appendChild(element);
    element = null;
  }
  rowId = rowId + 1;
}
function deleteRows(tableID) {
  var table = document.getElementById(tableID);
  var rowCount = table.rows.length;
  for(var i=0; i<rowCount; i++) {
    var row = table.rows[i];
    var chkbox = row.cells[0].childNodes[0];
    if(null != chkbox && true == chkbox.checked) {
          table.deleteRow(i);
          rowCount--;
          i--;
        }
  }
  calculate_total();
  calculate_total_value();
}


function deleteRows_new(tableID,table_data) {
      let quantityId = 'quantity'
      if ('quantityId' in table_data){
          quantityId = table_data.quantityId
      }
  var table = document.getElementById(tableID);
  var rowCount = table.rows.length;
  for(var i=0; i<rowCount; i++) {
    var row = table.rows[i];
    var chkbox = row.cells[0].childNodes[0];
    if(null != chkbox && true == chkbox.checked) {
          table.deleteRow(i);
          rowCount--;
          i--;
        }
  }
      if ('total_id' in table_data){
        calculate_total_new(table_data.total_id,quantityId)
    }
      else {calculate_total_new('total_quantity')}
      calculate_total_value();
    }



function add_shade_row(table_id, table_data, row_id){
  var table = document.getElementById(table_id);
  var row = table.insertRow(table.rows.length);
  var field, element, div, cellCount = 0;

  field = row.insertCell(cellCount++);
  element = document.createElement("input");
  element.type = "checkbox";
  element.className += '';
  field.appendChild(element);
  element = null;

  if(table_data.type === true){
    field = row.insertCell(cellCount++);
    element = document.createElement("input");
    element.type = "text";
    element.name = `line[${rowId}]` + "[type]";
    element.style.display = "none";
    element.value = document.getElementById("type"+row_id).value;
    field.appendChild(element);
    element = null;
  }

  if(table_data.item === true){
    field = row.insertCell(cellCount++);
    element = document.createElement("input");
    element.type = "text";
    element.name = `line[${rowId}]` + "[item_id]";
    element.style.display = "none";
    element.value = document.getElementById("item"+row_id).value;
    field.appendChild(element);
    element = null;
  }

  if(table_data.shade === true){
    field = row.insertCell(cellCount++);
    element = document.createElement("select");
    element.name = `line[${rowId}]` + "[shade_id]";
    element.className += ' form-control select2 table_select ';
    element.id = "shade" + rowId;
    element.required = true;
    element.style.width = "120px";
    field.appendChild(element);
    element = null;

    set_select_options("shade" + rowId, '/api/master/get-shades/?item_id=' + document.getElementById("item"+row_id).value);

    field = row.insertCell(cellCount++);
  }

  if(table_data.quantity === true){
       let quantityId = 'quantity'
      if ('quantityId' in table_data){
          quantityId = table_data.quantityId
      }
    field = row.insertCell(cellCount++);
    element = document.createElement("input");
    element.type = "text";
    element.name = `line[${rowId}]` + "[quantity]";
    element.className += 'form-control table_input_field ';
    element.placeholder = "Quantity"
    element.id = quantityId + rowId;
    element.required = true;
    element.style.width = "85px";
    element.addEventListener("keyup", function(event) {
      var thisrow = this.id.substring(8);
      calculate_amount(thisrow);
        if ('total_id' in table_data){
            calculate_total_new(table_data.total_id,quantityId)
        }
        else {calculate_total_new('total_quantity')}
      calculate_total_value();
    });
    field.appendChild(element);
    element = null;
  }

  if(table_data.rate === true){
    field = row.insertCell(cellCount++);
    element = document.createElement("input");
    element.type = "text";
    element.name = `line[${rowId}]` + "[rate]";
    element.id = "rate" + rowId;
    element.required = true;
    element.className += " form-control table_input_field ";
    element.value = document.getElementById("rate"+row_id).value;
    element.style.width = "85px";
    field.appendChild(element);
    element = null;
  }

  if(table_data.amount === true){
    field = row.insertCell(cellCount++);
    element = document.createElement("input");
    element.type = "text";
    element.name = `line[${rowId}]` + "[amount]";
    element.className += 'form-control table_input_field ';
    element.id = "amount" + rowId;
    element.required = true;
    element.placeholder = "Amount";
    element.value = 0;
    element.style.width = "85px";
    element.readOnly = true;
    field.appendChild(element);
    element = null;
  }
  if(table_data.description === true){
    field = row.insertCell(cellCount++);
    element = document.createElement("input");
    element.type = "text";
    element.name = `line[${rowId}]` + "[description]";
    element.className += 'form-control table_input_field';
    element.id = "description" + rowId;
    element.placeholder = "Description"
    field.appendChild(element);
    element = null;
  }
  rowId = rowId + 1;

}


function validate_table_selects(){
  for (select of document.getElementsByClassName("table_select")){
    if(select.options[select.selectedIndex].value=="-1"){
      alert("Slect an Option in all Selects!")
      return false;
    }
  }
  return true;
}

var get_int_value = function ( i ) {
    return typeof i === 'string' ?
        i.replace(/[\$,]/g, '')*1 :
        typeof i === 'number' ?
            i : 0;
};

