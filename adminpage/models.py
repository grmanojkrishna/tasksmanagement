from django.db import models
from django.contrib.auth.models import User


class Tasksdetails(models.Model):
    tasks = models.CharField(max_length=100, null=False, blank=False)
    checkbox = models.CharField(max_length=100, null=False, blank=False, default='unchecked')

    def __str__(self):
        return "{}".format(self.tasks)


