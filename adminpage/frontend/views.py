from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from adminpage.models import Tasksdetails


@login_required
def adminpage(request):
    context = dict()
    context['tasksdetails'] = Tasksdetails.objects.all().values("id", "tasks", "checkbox")
    return render(request=request, template_name="tasksmanagement/adminpage.html", context=context)

