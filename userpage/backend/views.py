from rest_framework.response import Response
from adminpage.models import Tasksdetails
from tasksmanagement.views import JSONApi


class Add1(JSONApi):
    @staticmethod
    def post(request):
        tasksdetails_data = request.data
        tasksdetails = Tasksdetails(tasks=tasksdetails_data['check'])
        tasksdetails.save()
        return Response({"id": tasksdetails.id})

