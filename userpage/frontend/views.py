from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from adminpage.models import Tasksdetails


@login_required
def userpage(request):
    context = dict()
    context['tasksdetails'] = Tasksdetails.objects.all().values("id", "tasks", "checkbox")
    return render(request=request, template_name="tasksmanagement/userpage.html", context=context)

