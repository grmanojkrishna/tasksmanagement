from rest_framework import parsers, permissions, authentication, views, renderers


class CSRFExemptSessionAuth:
    pass


class JSONApi(views.APIView):
    authentication_classes = (authentication.TokenAuthentication, CSRFExemptSessionAuth,)
    permission_classes = (permissions.IsAuthenticated,)
    renderer_classes = (renderers.JSONRenderer,)
    parser_classes = (parsers.JSONParser,)


